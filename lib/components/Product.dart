import 'package:flutter/material.dart';

class Product extends StatelessWidget {
  const Product({
    Key? key,
    required this.title,
    required this.subtitle,
    required this.icon
  }) : super(key: key);

  final String title;
  final String subtitle;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey.shade300))),
      child: ListTile(
            leading: Icon(icon),
            title: Text(title),
            subtitle: Text(subtitle),
            trailing: const Icon(Icons.add),
          ),
    );
  }
}