// List of product items
import 'package:flutter/material.dart';
import 'package:yasir/components/Product.dart';

class Products extends StatefulWidget {
  const Products({Key? key}) : super(key: key);

  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Products'),
      ),
      body: ListView(
        children: const <Widget>[
          Product(
            title: 'Es Batu',
            subtitle: 'Air putih beku keras seperti batu',
            icon: Icons.ac_unit
          ),
          Product(
            title: 'Kecap ABC',
            subtitle: 'Sari kedelai hitam merek ABC',
            icon: Icons.abc
          ),
          Product(
            title: 'Tambah Nasi',
            subtitle: 'Siapa tau masih lapar',
            icon: Icons.rice_bowl
          ),
        ]
      )
    );
  }
}
    
    